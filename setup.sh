# create networks
docker network create proxy
docker network create minio
docker network create postgres

# traefik acme.json
sudo mkdir -p /etc/traefik
sudo touch /etc/traefik/acme.json
sudo chmod 600 /etc/traefik/acme.json
