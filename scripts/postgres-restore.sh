# stop db
docker compose --env-file .env --project-directory docker/postgres stop postgres
# remove data
docker volume rm -f postgres_data
# fetch latest backup
docker compose --env-file .env --project-directory docker/postgres run --rm postgres sh -c '/wal-g backup-fetch $PGDATA LATEST; touch $PGDATA/recovery.signal'
# start db
docker compose --env-file .env --project-directory docker/postgres up -d postgres
